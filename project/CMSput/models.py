from django.db import models


class Contenido(models.Model):
    key = models.CharField(max_length=64)
    value = models.TextField(null=True)

