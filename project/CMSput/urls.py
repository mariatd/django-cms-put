from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('<str:llave>/', views.get_content, name='get_content'),
]
