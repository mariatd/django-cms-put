from django.shortcuts import render
from django.http import HttpResponse
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt


def home(request):
    return HttpResponse("¡Página de inicio de CMSput!")


form = """"
No hay asignado un valor . Pon uno:
<form action="" method="POST">
    Valor: <input type="text" name="valor">
    <br/><input type="submit" value"Enviar">
</form>
"""


@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
        c = Contenido(key=llave, value=valor)
        c.save()
    elif request.method == "POST":
        valor = request.POST['valor']
        c = Contenido(key=llave, value=valor)
        c.save()
    try:
        contenido = Contenido.objects.get(key=llave)
        respuesta = contenido.value
    except Contenido.DoesNotExist:
        respuesta = form
    return HttpResponse(respuesta)